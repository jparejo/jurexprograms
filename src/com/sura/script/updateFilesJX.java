package com.sura.script;

import com.sura.conexion.conexSQLServer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.*;

public class updateFilesJX {


    public static Integer contleidos;

    public static Integer contupdate;

    public static void updatePpal () throws IOException, SQLException {

        String fileInput = "/home/jparejo/Documentos/Jurex/procesos-batch/malvaro20210119.txt";
        // String fileInput = "/home/jesus/sinisterYearFiscalWFTesting.txt";
        String[] fieldSise = null;
        String numeroAsunto = null;
        String idResponsable = null;
        String TableDB, fieldDb = null;
        String fieldUpdate = null;
        Integer valueField = null;
        int actUpd;
        conexSQLServer conex = new conexSQLServer();
        Connection conexS = conex.openConnectionSinister();
        if (conexS != null) {
            System.out.println("conexion activa");
        }

        // System.exit(0);

        contleidos = 0;
        contupdate = 0;

        /** Lee el archivo de entrada   **/
        String fileWFSise;
        FileReader f = new FileReader(fileInput);
        BufferedReader b = new BufferedReader(f);
        while ((fileWFSise = b.readLine()) != null) {

            fieldSise = fileWFSise.split("\\|");
            numeroAsunto = fieldSise[0];
            idResponsable = fieldSise[1];

           /* System.out.println("NumeroAsunto " + numeroAsunto);
            System.out.println("idResponsable= " + idResponsable);*/


            contleidos = contleidos + 1;

            TableDB = "jurex_dev.dbo.tbl_asuntos";
            fieldDb = "asu_id";

            Integer nroAsunto = Integer.parseInt(numeroAsunto);
            System.out.println("Numero de Asunto= " + nroAsunto);

            Integer idrespon = Integer.parseInt(idResponsable);
            System.out.println("Responsable= " + idrespon);


            actUpd = 0;

            fieldUpdate = "asu_tab_resp2_id";
            valueField = idrespon;
            actUpd = updateDataAsunto(nroAsunto, conexS, TableDB, fieldDb, fieldUpdate, valueField);

            if (actUpd == 1) {
                contupdate = contupdate + 1;
                System.out.println("Actualizados");
            } else {
                System.out.println("No Actualizados");
            }
            System.out.println("NumeroAsunto " + numeroAsunto);
            System.out.println("idResponsable= " + idResponsable);

        }

        b.close();
        conexS.close();   // Cierro la conexion de complains
        System.out.println("Cantidad de registros leidos: " + contleidos);
        System.out.println("Cantidad de registros leidos actualizados: " + contupdate);
    }


    public static int updateDataAsunto(Integer nroAsunto, Connection conexSql, String TableDB, String fieldDb, String fieldUpdate, Integer valueField) throws SQLException {

        String strSqlUpdate = null;
        int contFounds = 0;
        strSqlUpdate = "UPDATE " + TableDB + " SET " + fieldUpdate + " = " + valueField + " WHERE " + fieldDb + " = " + nroAsunto + ";";
        // strSqlQuery = strSqlQuery + sinisterNumber;
        System.out.println(strSqlUpdate);

        PreparedStatement stmt = conexSql.prepareStatement(strSqlUpdate);

        int returnUpdate = stmt.executeUpdate();

        return returnUpdate;

    }


}
