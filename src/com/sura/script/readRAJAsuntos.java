package com.sura.script;

import com.sura.conexion.conexSQLServer;

import java.io.*;
import java.sql.*;

public class readRAJAsuntos {


    public static Integer contleidos;

    public static Integer contupdate;

    public static void readJurexForRajNumber() throws IOException, SQLException {

        String fileInput = "/home/jparejo/Documentos/Jurex/Conciliacion_RAJ/raj_el_comercio_id.txt";
        String fileOutput = "/home/jparejo/Documentos/Jurex/Conciliacion_RAJ/raj_el_comercio_id-salida20201030.csv";
        // String fileInput = "/home/jesus/sinisterYearFiscalWFTesting.txt";
        /** Campos del Archivo de Entrada*/
        String[] fieldSise = null;
        String idJurex = null;
        String tipoJurex = null;
        String rajJurex = null;
        String seccionJurex = null;
        String ejercicioJurex = null;
        String siniestroJurex = null;
        String itemJurex = null;
        String reclamoJurex = null;

        String idResponsable = null;
        String TableDB, fieldDb, fieldDb2, valueField2 = null;
        String fieldUpdate, SalidaAsunto, SalidaFinal, SalidaAsunto2 = null;
        Integer valueField = null;
        Integer valueTemp, numeroAsunto = null;

        int actUpd;
        conexSQLServer conex = new conexSQLServer();
        Connection conexS = conex.openConnectionSinister();
        if (conexS != null) {
            System.out.println("conexion activa");
        }

        // System.exit(0);

        contleidos = 0;
        contupdate = 0;

        /** Lee el archivo de entrada   **/
        String fileWFSise;
        FileReader f = new FileReader(fileInput);
        BufferedReader b = new BufferedReader(f);

        /**
         * genera el archivo de salida.
         */
        File fileSal = new File(fileOutput);
        FileWriter fwSal = new FileWriter(fileSal);
        BufferedWriter bwSAl = new BufferedWriter(fwSal);

        while ((fileWFSise = b.readLine()) != null) {

            System.out.println("file" + fileWFSise);

            fieldSise = fileWFSise.split("\\|");

            idJurex = fieldSise[0];
            tipoJurex = fieldSise[1];
            rajJurex = fieldSise[2];
            seccionJurex = fieldSise[3];
            ejercicioJurex = fieldSise[4];
            siniestroJurex = fieldSise[5];
            itemJurex = fieldSise[6];
            reclamoJurex = fieldSise[7];


            System.out.println("Id Jurex " + idJurex);
            System.out.println("Tipo Jurex " + tipoJurex);
            System.out.println("Raj Jurex " + rajJurex);
            System.out.println("Seccion Jurex " + seccionJurex);
            System.out.println("Ejercicio Jurex " + ejercicioJurex);
            System.out.println("Siniestro Jurex " + siniestroJurex);
            System.out.println("Item Jurex " + itemJurex);
            System.out.println("reclamo Jurex " + reclamoJurex);

            contleidos = contleidos + 1;

            TableDB = "jurex_dev.dbo.TBL_ADICIONALES_ASUNTOS";
            fieldDb = "ADA_TAB_TIPO_ID";
            fieldDb2 = "ADA_DATO_DS";
            valueField = 49975;    // CAmpo RAJ

            valueTemp = Integer.parseInt(rajJurex);

            if (valueTemp < 100000) {

                if (valueTemp < 10000) {
                    rajJurex = "0000" + rajJurex;
                } else {
                    rajJurex = "000" + rajJurex;
                }

                System.out.println("Raj Jurex modificado " + rajJurex);
            }

            valueField2 = rajJurex;

            actUpd = 0;

            numeroAsunto = 0;

            numeroAsunto = getNumberAsunto(conexS, TableDB, fieldDb, fieldDb2, valueField, valueField2);

            System.out.println("Numero de Asunto: " + numeroAsunto);
            SalidaAsunto = null;
            if (numeroAsunto > 0) {
                fieldDb = "ADA_ASU_ID";
                SalidaAsunto = getDatosAsuntos(conexS, TableDB, fieldDb, numeroAsunto);
            } else {

                SalidaAsunto = "No existe Raj en Jurex";
            }


            /**
             * Busco los productos del asunto.
             */
            // TableDB = "tbl_asuntos";
            fieldDb = "ta.ASU_ID";
            SalidaAsunto2 = getDatosProductos(conexS, TableDB, fieldDb, numeroAsunto, SalidaAsunto);

            if ( (SalidaAsunto2).equals("") ) {
                System.out.println("Error" + numeroAsunto);
            } else {
                SalidaFinal = null;
                SalidaFinal = idJurex + "|" + tipoJurex + "|" + rajJurex + "|" + seccionJurex + "|" + ejercicioJurex + "|" + siniestroJurex + "|" + itemJurex + "|" + reclamoJurex;
                SalidaFinal = SalidaFinal + "|" + numeroAsunto + "|" + SalidaAsunto2;
                System.out.println("SalidaFinal = " + SalidaFinal);

                bwSAl.write(SalidaFinal);
                bwSAl.newLine();
                contupdate = contupdate + 1;
            }
        }

        b.close();
        bwSAl.close();
        conexS.close();   // Cierro la conexion de complains
        System.out.println("Cantidad de registros leidos: " + contleidos);
        System.out.println("Cantidad de registros leidos en el archivo: " + contupdate);
    }

    /**
     * Metodo para retornar el numero de asunto dado el RAJ de SISE para listar los datos de las tablas de datos de Jurex.
     *
     * @param conexSql
     * @param TableDB
     * @param fieldDb
     * @param fieldDb2
     * @param valueField
     * @param valueField2
     * @return numeroAsunto: numero de id de la tabla de tareas.
     */
    public static Integer getNumberAsunto(Connection conexSql,
                                          String TableDB,
                                          String fieldDb,
                                          String fieldDb2,
                                          Integer valueField,
                                          String valueField2) {


        String sqlSelect = null;
        Statement stmt = null;
        Integer numeroAsunto = 0;
        Integer existe = 0;
        Integer idTablaTareasUltUpd = 0;

        System.out.println("");
        System.out.println("Tabla " + TableDB);
        // System.out.println("DesTabla " + desTabla);

        sqlSelect = "SELECT ADA_ASU_ID as NroAsunto FROM " + TableDB + " WHERE " + fieldDb + "= " + valueField + " and " + fieldDb2 + " = '" + valueField2 + "'";

        System.out.println(sqlSelect);

        try {

            stmt = conexSql.createStatement();
            ResultSet rs = stmt.executeQuery(sqlSelect);



            while (rs.next()) {

                numeroAsunto = rs.getInt("NroAsunto");
                existe = 1;
                break;


            }

            rs.close();
            stmt.close();

            return numeroAsunto;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return 0;   // no consegui datos del asunto.
    }

    /**
     * @param conexSql
     * @param TableDB
     * @param fieldDb
     * @param valueField
     * @return
     */
    public static String getDatosAsuntos(Connection conexSql,
                                         String TableDB,
                                         String fieldDb,
                                         Integer valueField) {


        String sqlSelect = null;
        Statement stmt = null;

        Integer idTablaTareasUltUpd = 0;
        String SalidaLectura = null;
        Integer numeroAsunto = 0;
        String datoDS, datoDX = null;
        Integer tipoDS = null;

        String NroSise = null;
        String NroRaj = null;
        String FechaRaj = null;
        String NroSiniestro = null;
        String Item = null;

        System.out.println("");
        System.out.println("Tabla " + TableDB);
        // System.out.println("DesTabla " + desTabla);

        sqlSelect = "SELECT taa.ADA_ASU_ID as NroAsunto," +
                " taa.ADA_TAB_TIPO_ID AS tipoDatoAdicional," +
                " taa.ADA_DATO_DS as contentDatoDS, " +
                " taa.ADA_DATO_TX as contentDatoTx," +
                " ttd.TAB_DESCRIPCION_DS as DescripcionCampo" +
                " FROM " + TableDB + " taa, TBL_TABLAS_DATOS ttd WHERE " + fieldDb + "= " + valueField + " and taa.ADA_TAB_TIPO_ID = ttd.TAB_ID";

        System.out.println(sqlSelect);

        try {

            stmt = conexSql.createStatement();
            ResultSet rs = stmt.executeQuery(sqlSelect);

            while (rs.next()) {

                numeroAsunto = rs.getInt("NroAsunto");
                datoDS = rs.getString("contentDatoDS");
                tipoDS = rs.getInt("tipoDatoAdicional");
                datoDX = rs.getString("contentDatoTx");


                int day = 5;
                String dayString;

                // instrucción switch con tipo de datos int
                switch (tipoDS) {
                    case 49975:
                        NroRaj = datoDS;
                        break;
                    case 49069:
                        NroSise = datoDS;
                        break;
                    case 49079:
                        FechaRaj = datoDS;
                        break;
                    case 49281:
                        NroSiniestro = datoDS;
                        break;
                    case 49113:
                        Item = datoDS;
                        break;
                    default:
                        break;
                }
            }

            rs.close();
            stmt.close();

            SalidaLectura = NroRaj + "|" + NroSise + "|" + FechaRaj + "|" + NroSiniestro + "|" + Item + "|";
            return SalidaLectura;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;   // no consegui datos del asunto.
    }

    /**
     * Busca los productos de los asuntos.
     * @param conexSql
     * @param TableDB
     * @param fieldDb
     * @param valueField
     * @param SalidaAsuntoProductos
     * @return Devuelve la traza con los productos de un asunto.
     */





    public static String getDatosProductos(Connection conexSql,
                                         String TableDB,
                                         String fieldDb,
                                         Integer valueField,
                                         String SalidaAsuntoProductos) {


        String sqlSelect = null;
        Statement stmt = null;

        String SalidaCiclo = null;
        String numeroOperacion = null;
        String DescripcionP = null;
        String SubSiniestro = null;
        Integer indTraza, indAct = 0;

        System.out.println("");
        System.out.println("Tabla " + TableDB);
        // System.out.println("DesTabla " + desTabla);

        sqlSelect =  "Select tp.PRO_OPERACION_DS as Operacion, tp.PRO_DESCRIPCION_DS as DescripcionProd, tap.ADR_DATO_DS as SubSiniestro " +
                     "from TBL_ASUNTOS ta, TBL_PRODUCTOS tp, REL_PRODUCTOS_ASUNTOS rpa, TBL_ADICIONALES_PRODUCTOS tap, TBL_TABLAS_DATOS ttd " +
                     "where "+ fieldDb + "= "+ valueField +" and " +
                            " rpa.RPA_ASU_ID = ta.ASU_ID " +
                            "and tap.ADR_PRO_ID = rpa.RPA_PRO_ID " +
                            "and tp.PRO_ID = rpa.RPA_PRO_ID " +
                            "and tap.ADR_TAB_TIPO_ID = ttd.TAB_ID";

        System.out.println(sqlSelect);

        try {

            stmt = conexSql.createStatement();
            ResultSet rs = stmt.executeQuery(sqlSelect);

            indAct = 0;
            indTraza = 0;

            while (rs.next()) {

                numeroOperacion = rs.getString("Operacion");
                DescripcionP = rs.getString("DescripcionProd");
                SubSiniestro = rs.getString("SubSiniestro");

                /*System.out.println("Operacion " + numeroOperacion);
                System.out.println("Descripcion " + DescripcionP);
                System.out.println("SubSiniestro " + SubSiniestro);*/

                if ( (numeroOperacion == null) || (numeroOperacion.isEmpty()) ) {
                    numeroOperacion = "S/N";
                }

                if ( (DescripcionP == null) || (DescripcionP.isEmpty()) ) {
                    DescripcionP = "S/N";
                }

                if ((SubSiniestro == null) || (SubSiniestro.isEmpty())) {

                  SubSiniestro = "S/N";
                }

                if (indTraza == 0) {
                    SalidaCiclo = numeroOperacion + "|" + DescripcionP + "|" + SubSiniestro + "|";
                    indTraza = 1;
                } else {
                    SalidaCiclo = SalidaCiclo + numeroOperacion + "|" + DescripcionP + "|" + SubSiniestro + "|";
                }

                indAct = 1;

            }

            if (indAct == 0) {
                SalidaCiclo =  "No Tiene Productos|";
            }

            rs.close();
            stmt.close();

            SalidaAsuntoProductos = SalidaAsuntoProductos + SalidaCiclo;
            return SalidaAsuntoProductos;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;   // no consegui datos del asunto.
    }


}
