package com.sura.script;


import com.sura.conexion.conexSQLServer;

import java.io.*;
import java.math.BigInteger;
import java.sql.*;


public class newTaskJurex {

    static final String descriptionTask = "Se relevó con IT reserva 0 se modifica reserva por pauta de apertura o estimación según corresponda";
    static final Integer tabIntervinienteID = 1520179;
    static final String dateTask = "'2020-11-11 00:00:00'";
    static final Integer tabTaksTypeID = 8606;
    static final Integer tabExtID = 9010;

    public static void newTaskInsert() throws IOException, SQLException {

        /**
         * Comienza el proceso de lectura y actualizacion
         */

        String fileInput = "/home/jparejo/Documentos/Jurex/procesos-batch/insertTaskACalzada.txt";
        String rutaSalida = "/home/jparejo/Documentos/Jurex/procesos-batch/insertTaskACalzada-20201111.txt";

        String[] fieldJurex = null;
        String numeroAsunto = null;
        String TableDB, nbField = null;
        String desTabla = null;
        Integer NumAsunto = 0;
        Integer numTabID = 0;
        Integer salInsert = 0;
        int contActualizados = 0, contLeidos = 0;
        String salidaTXT = null;

        System.out.println("Paso a crear la tarea");
        conexSQLServer conex = new conexSQLServer();
        Connection conexS = conex.openConnectionSinister();
        if (conexS != null) {
            System.out.println("conexion activa");
        }

        /**
         * Lee archivo de entrada de Asuntos.
         */
        /** Lee el archivo de entrada   **/
        String fileWFSise;
        FileReader f = new FileReader(fileInput);
        BufferedReader b = new BufferedReader(f);
        /**
         * genera el archivo de salida.
         */
        File file = new File(rutaSalida);
        FileWriter fw = new FileWriter(rutaSalida);
        BufferedWriter bw = new BufferedWriter(fw);
        while ((fileWFSise = b.readLine()) != null) {

            fieldJurex = fileWFSise.split("\\|");
            numeroAsunto = fieldJurex[0];
            NumAsunto = Integer.parseInt(numeroAsunto);
            System.out.println("numero de asunto: " + numeroAsunto);
            contLeidos = contLeidos + 1;

            /**
             * Guarda y busca el numero de secuencia de la tabla de tareas.
             */
            TableDB = "jurex_dev.dbo.TBL_NUMEROS";
            desTabla = "'TBL_TAREAS'";
            nbField = "NUM_ID ";

            numTabID = getNumberTableTaskID(conexS, TableDB, desTabla, nbField);

            /**
             * Inicializa campo de la tabla.
             */
            TableDB = "jurex_dev.dbo.tbl_tareas";
            salInsert = 0; // Si cambia a uno actualizo.
            salInsert = insertTaskAsunto(NumAsunto,
                    numTabID,
                    conexS,
                    descriptionTask,
                    tabIntervinienteID,
                    dateTask,
                    tabTaksTypeID,
                    tabExtID,
                    TableDB);

            System.out.println("salInsert: " + salInsert);

            if (salInsert == 1) {
                salidaTXT = "Nro Asunto: " + NumAsunto + "|NumeroTarea: " + numTabID + "|Descripcion: " + descriptionTask + "| Di Pace, Marcelo" + "|Fecha de Registro: " + dateTask + "|Tipo Tarea: " + tabTaksTypeID + '\n';
                System.out.println(salidaTXT);
                bw.write(salidaTXT);
                contActualizados = contActualizados + 1;
            } else {
                System.out.println("Error" + NumAsunto);
            }

        }

        b.close();
        bw.close();
        conexS.close();

        System.out.println("Total Actualizados:" + contActualizados);
        System.out.println("Total Leidos: " + contLeidos);


    }

    /**
     * Metodo poara insertar la nueva tarea.
     *
     * @param nroAsunto
     * @param tarID
     * @param conexSql
     * @param descriptionTask
     * @param tabIntervinienteID
     * @param dateTask
     * @param tabTaksTypeID
     * @param tabExtID
     * @param TableDB
     * @return
     * @throws SQLException
     */

    public static int insertTaskAsunto(Integer nroAsunto,
                                       Integer tarID,
                                       Connection conexSql,
                                       String descriptionTask,
                                       Integer tabIntervinienteID,
                                       String dateTask,
                                       Integer tabTaksTypeID,
                                       Integer tabExtID,
                                       String TableDB) throws SQLException {

        String strSqlInsert = null;
        int returnUpdate = 0;

        strSqlInsert = "INSERT INTO " + TableDB +
                "(TAR_ID, TAR_ASU_ID, TAR_FECHA_DT, TAR_TAB_INTERVINIENTE_ID, TAR_DESCRIPCION_DS, TAR_TAB_TIPO_TAREA_ID, TAR_FACTURABLE_FL, TAR_TIEMPO_NE, TAR_TAB_EXT_ID, TAR_ORIGEN_NE, TAR_EXISTEN_ADICIONALES_FL) " +
                "VALUES(" + tarID + "," + nroAsunto + "," + dateTask + "," + tabIntervinienteID + "," + "'" + descriptionTask + "'" + "," + tabTaksTypeID + "," + 0 + "," + 0 + "," + tabExtID + ", 0, 0);";

        System.out.println(strSqlInsert);

        PreparedStatement stmt = conexSql.prepareStatement(strSqlInsert);

        returnUpdate = stmt.executeUpdate();

        return returnUpdate;

    }

    /**
     * Metodo para retornar el numero de id para actualizar la tabla de Tareas.
     *
     * @param conexSql
     * @param TableDB
     * @param desTabla
     * @param nbField
     * @return idTablaTareasUltUpd: numero de id de la tabla de tareas.
     */
    public static int getNumberTableTaskID(Connection conexSql,
                                           String TableDB, String desTabla, String nbField) {
        String sqlSelect = null;
        Statement stmt = null;
        Integer idTablaTareasUlt = 0;
        Integer idTablaTareasUltUpd = 0;

        System.out.println("");
        System.out.println("Tabla " + TableDB);
        System.out.println("DesTabla " + desTabla);

        sqlSelect = "SELECT * FROM " + TableDB + " WHERE " + nbField + "= " + desTabla;

        System.out.println(sqlSelect);

        try {

            stmt = conexSql.createStatement();
            ResultSet rs = stmt.executeQuery(sqlSelect);

            while (rs.next()) {

                idTablaTareasUlt = rs.getInt("NUM_PROXIMO_NE");

                idTablaTareasUltUpd = idTablaTareasUlt + 1;

                System.out.println("Antes " + idTablaTareasUlt);
                System.out.println("Despues " + idTablaTareasUltUpd);

                /**
                 * Realiza el update de la tabla de TBL_Numeros para poder actualizar la tabla de tareas.
                 */
                updateNumeroTablaIDTask(conexSql, idTablaTareasUltUpd, desTabla, nbField, TableDB);

                break;


            }

            rs.close();
            stmt.close();

            return idTablaTareasUltUpd;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return idTablaTareasUltUpd;
    }

    /**
     * Metodo que actualiza el registro de numeros de la tabla de tareas.
     * @param conexSql
     * @param idTablaTareasUltUpd
     * @param desTabla
     * @param nbField
     * @param TableDB
     * @return
     * @throws SQLException
     */

    public static int updateNumeroTablaIDTask(Connection conexSql,
                                              Integer idTablaTareasUltUpd,
                                              String desTabla,
                                              String nbField,
                                              String TableDB) throws SQLException {
        int indAct = 0;
        int returnUpdate = 0;
        String strSqlUpdate = null;
        strSqlUpdate = "UPDATE jurex_dev.dbo.TBL_Numeros SET Num_Proximo_NE = " + idTablaTareasUltUpd + " WHERE " + nbField + "= " + desTabla;
        System.out.println(strSqlUpdate);
        PreparedStatement stmt = conexSql.prepareStatement(strSqlUpdate);
        returnUpdate = stmt.executeUpdate();
        return returnUpdate;

    }

}
