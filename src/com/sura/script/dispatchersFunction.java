package com.sura.script;

import java.io.IOException;
import java.sql.SQLException;

public class dispatchersFunction {

    public static Integer dispFunc;

    public static void main(String[] args) throws IOException, SQLException {

        dispFunc = 4;

        switch (dispFunc) {

            case 1:

                System.out.println("Opcion 1");

                updateFilesJX.updatePpal();

                break;

            case 2:

                System.out.println("Opcion 2");

                newTaskJurex.newTaskInsert();

                break;

            case 3:

                System.out.println("Opcion 3");

                readRAJAsuntos.readJurexForRajNumber();

                break;

            case 4:

                System.out.println("Opcion 4");

                readRAJAsuntosBeta.readJurexForRajNumberBeta();

                break;

            default:

                System.out.println("Opcion no existe");

                break;


        }


    }
}


